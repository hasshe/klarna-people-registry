package controller;

import models.Child;
import models.OldestChildResponse;
import models.Person;
import models.Spouse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PersonsRegistryControllerTest {

    PersonsRegistryController personsRegistryController = new PersonsRegistryController();

    public Person personWithAllInfo = new Person("Joe", new Spouse("Jane"), Set.of(
            new Child(12, "Max"),
            new Child(15, "Chris"),
            new Child(19, "Milan")));
    public Person personWithSpouseAndNoChildren = new Person("Theo", new Spouse("Elsa"), Set.of());
    public Person personWithChildrenAndNoSpouse = new Person("Rafael", null, Set.of(
            new Child(3, "Elijah")));
    public Person personWithMinimalInfo = new Person("Ismael", null, Set.of());

    @BeforeEach
    void setUp() {
        personsRegistryController.addPerson(1L, personWithAllInfo);
        personsRegistryController.addPerson(2L, personWithSpouseAndNoChildren);
        personsRegistryController.addPerson(3L, personWithChildrenAndNoSpouse);
        personsRegistryController.addPerson(4L, personWithMinimalInfo);
    }

    @AfterEach
    void tearDown() {
        PersonsRegistryController.personRegistry.clear();
    }

    @Test
    void addPerson() {
        Person personToAdd = new Person("Simon", new Spouse("Camilla"), Set.of(new Child(2, "Lisa")));

        Person actual = personsRegistryController.addPerson(5L, personToAdd);

        assertEquals(actual, personToAdd);
    }

    @Test
    void getPersonBySSN() {
        Person actual = personsRegistryController.getPersonBySSN(2L);

        assertEquals(actual.spouse().name(), "Elsa");
        assertEquals(actual.name(), "Theo");
    }

    @Test
    void getPersonsOldestChild() {
        OldestChildResponse oldestChildResponse = personsRegistryController.getPersonsOldestChildBySSN(1L);

        Child oldestChild = personWithAllInfo.getOldestChild();

        assertEquals(oldestChildResponse.oldestChild().age(), oldestChild.age());
        assertEquals(oldestChildResponse.oldestChild().age(), 19);
    }

    @Test
    void tryAddPersonWithInvalidSSN() {
        Person personToAdd = new Person("Simon", new Spouse("Camilla"), Set.of(new Child(2, "Lisa")));

        assertThrows(IllegalArgumentException.class, () -> personsRegistryController.addPerson(null, personToAdd));
    }

    @Test
    void tryAddPersonWithNullRequestBody() {
        assertThrows(IllegalArgumentException.class, () -> personsRegistryController.addPerson(5L, null));
    }

    @Test
    void tryAddPersonWithIdenticalSSN() {
        Person personToAdd = new Person("Simon", new Spouse("Camilla"), Set.of(new Child(2, "Lisa")));

        assertThrows(IllegalArgumentException.class, () -> personsRegistryController.addPerson(4L, personToAdd));
    }

    @Test
    void tryGetPersonThatDoesNotExist() {
        assertThrows(IllegalArgumentException.class, () -> personsRegistryController.getPersonBySSN(100L));
    }

    @Test
    void tryGetPersonWithNullSSN() {
        assertThrows(IllegalArgumentException.class, () -> personsRegistryController.getPersonBySSN(null));
    }

    @Test
    void tryGetPersonsOldestChildWithNullSSN() {
        assertThrows(IllegalArgumentException.class, () -> personsRegistryController.getPersonsOldestChildBySSN(null));
    }

    @Test
    void tryGetPersonsOldestChildWithNoChildrenPresent() {
        assertThrows(IllegalArgumentException.class, () -> personsRegistryController.getPersonsOldestChildBySSN(2L));
    }

    @Test
    void returnNullWhenFetchingOldestChildForPersonWithNoChildren() {
        assertNull(personWithSpouseAndNoChildren.getOldestChild());
    }
}