package models;

public record Spouse(String name) {

    public Spouse {
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Name cannot be empty");
        }
    }
}
