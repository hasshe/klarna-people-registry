package models;

import java.util.Set;

public record Person(String name, Spouse spouse, Set<Child> children) {

    public Person {
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Name cannot be empty");
        }
    }

    public Child getOldestChild() {
        if (children.isEmpty()) {
            return null;
        }
        int ageOfOldestChild = 0;
        Child oldestChild = null;
        for (Child child : children) {
            if (child.age() > ageOfOldestChild) {
                ageOfOldestChild = child.age();
                oldestChild = child;
            }
        }
        return oldestChild;
    }
}
