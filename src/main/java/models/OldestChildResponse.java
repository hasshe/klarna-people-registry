package models;

public record OldestChildResponse(Child oldestChild, Long parentSSN) {

    public OldestChildResponse {
        if (oldestChild == null) {
            throw new IllegalArgumentException(String.format("Invalid input. Data missing for child with parent SSN: %d.", parentSSN));
        }
    }
}
