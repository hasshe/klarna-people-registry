package models;

public record Child(int age, String name) {

    public Child {
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Name cannot be empty");
        }
        if (age < 0) {
            throw new IllegalArgumentException(String.format("%d is invalid age!", age));
        }
    }
}
