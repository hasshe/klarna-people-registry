package application;

import controller.PersonsRegistryController;
import models.Person;

import java.util.Set;

public class Application {

    public static void main(String[] args) {
        var person = new Person("Hassan", null, Set.of());
        PersonsRegistryController personsRegistryController = new PersonsRegistryController();
        System.out.println(person);
    }
}
