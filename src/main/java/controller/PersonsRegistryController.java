package controller;

import models.Child;
import models.OldestChildResponse;
import models.Person;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PersonsRegistryController {

    static final Map<Long, Person> personRegistry = new HashMap<>();
    private static final Logger logger = Logger.getLogger(PersonsRegistryController.class.getName());

    public Person addPerson(final Long ssn, Person person) {
        validateObject(ssn);
        validateObject(person);
        if (personRegistry.containsKey(ssn)) {
            logger.log(Level.WARNING, String.format("Person with SSN %d already exists.", ssn));
            throw new IllegalArgumentException(String.format("Person with SSN %d already exists.", ssn));
        }
        logger.log(Level.INFO, String.format("Adding new person: %s", person));
        personRegistry.put(ssn, person);
        return person;
    }

    public Person getPersonBySSN(final Long ssn) {
        validateObject(ssn);
        if (personRegistry.containsKey(ssn)) {
            logger.log(Level.INFO, String.format("Fetching Person with SSN %d.", ssn));
            return personRegistry.get(ssn);
        }
        logger.log(Level.WARNING, String.format("Person with SSN %d does not exists.", ssn));
        throw new IllegalArgumentException(String.format("Person with SSN %s does not exist.", ssn));
    }

    //Assumption: We don't care about the possibility of there being twins at the same age.
    //Whichever is chosen should be valid as a response.
    public OldestChildResponse getPersonsOldestChildBySSN(final Long ssn) {
        validateObject(ssn);
        Child oldestChild = getPersonBySSN(ssn).getOldestChild();
        if (oldestChild == null) {
            logger.log(Level.INFO, String.format("Person with SSN %d does not have any children registered.", ssn));
            throw new IllegalArgumentException(String.format("Person with SSN %d does not have any children registered.", ssn));
        }
        return new OldestChildResponse(oldestChild, ssn);
    }

    private void validateObject(Object obj) {
        if (obj == null) {
            logger.log(Level.WARNING, "Illegal null value encountered.");
            throw new IllegalArgumentException("Illegal null value encountered.");
        }
    }

}
